/* Copyright (C) 2021-2023 Stephan Kreutzer
 *
 * This file is part of JTeroTeroStA.
 *
 * JTeroTeroStA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * JTeroTeroStA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with JTeroTeroStA. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/JTeroTeroStA.java
 * @brief Demo program.
 * @author Stephan Kreutzer
 * @since 2021-11-11
 */



import java.io.File;
import java.io.InputStream;
import java.io.FileInputStream;
import java.util.Map;
import java.util.List;
import java.util.ArrayList;
import org.publishing_systems._20140527t120137z.jteroterosta.JTeroLoader;
import org.publishing_systems._20140527t120137z.jteroterosta.JTeroTero;
import org.publishing_systems._20140527t120137z.jteroterosta.JTeroPattern;
import org.publishing_systems._20140527t120137z.jteroterosta.JTeroFunction;
import org.publishing_systems._20140527t120137z.jteroterosta.JTeroStAInterpreter;
import org.publishing_systems._20140527t120137z.jteroterosta.JTeroEvent;
import org.publishing_systems._20140527t120137z.jteroterosta.JTeroInputStreamInterface;
import org.publishing_systems._20140527t120137z.jteroterosta.JTeroInputStreamStd;
import org.publishing_systems._20140527t120137z.jteroterosta.JTeroException;



public class JTeroTeroStA
{
    public static void main(String args[])
    {
        System.out.print("JTeroTeroStA Copyright (C) 2021-2023 Stephan Kreutzer\n" +
                         "This program comes with ABSOLUTELY NO WARRANTY.\n" +
                         "This is free software, and you are welcome to redistribute it\n" +
                         "under certain conditions. See the GNU Affero General Public License 3\n" +
                         "or any later version for details. Also, see the source code repository\n" +
                         "https://gitlab.com/publishing-systems/JTeroTeroStA/ and\n" +
                         "the project website https://publishing-systems.org.\n\n");

        try
        {
            if (args.length >= 1)
            {
                File inputFile = new File(args[0]);

                if (inputFile.exists() != true)
                {
                    System.out.println("File '" + args[0] + "' doesn't exist.");
                    return;
                }

                if (inputFile.isFile() != true)
                {
                    System.out.println("Path '" + args[0] + "' isn't a file.");
                    return;
                }

                if (inputFile.canRead() != true)
                {
                    System.out.println("File '" + args[0] + "' isn't readable.");
                    return;
                }

                InputStream in = new FileInputStream(inputFile);

                Run(in);
            }
            else
            {
                /** @todo: Support JTeroTeroStA <input-file> <code-directory> [<code-directory>...] into args. */
                System.out.print("Usage:\n\n\tJTeroTeroStA <input-file>\n\n");
            }
        }
        catch (Exception ex)
        {
            System.out.println("Exception: " + ex.getMessage());
            ex.printStackTrace();

            return;
        }
    }

    public static int Run(InputStream inputStream) throws JTeroException
    {
        JTeroInputStreamInterface stream = new JTeroInputStreamStd(inputStream);

        List<JTeroLoader> loaders = new ArrayList<JTeroLoader>();
        loaders.add(new JTeroLoader("./Cpp/"));
        loaders.add(new JTeroLoader("./FilterCpp/"));

        if (true)
        {
            JTeroTero tero = new JTeroTero(loaders, null);

            tero.parse(stream);

            System.out.print("\n");
        }
        else
        {
            JTeroTero tero = new JTeroTero(loaders, stream);

            while (tero.hasNext() == true)
            {
                JTeroEvent teroEvent = tero.nextEvent();

                System.out.println(teroEvent.getCurrentFunctionName() +
                                   " -" + teroEvent.getCurrentFunctionRepeatPatternName() + "|" + teroEvent.getCurrentFunctionRepeatElseCase() + "/" +
                                   teroEvent.getNextFunctionPatternName() + "|" + teroEvent.getNextFunctionElseCase() + "-> " +
                                   teroEvent.getNextFunctionName() + "," +
                                   teroEvent.getCurrentCount() + "," + teroEvent.getNextCount() +
                                   ": \"" + teroEvent.getData() + "\"");
            }
        }

        return 0;
    }
}

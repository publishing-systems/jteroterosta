/* Copyright (C) 2021-2022 Stephan Kreutzer
 *
 * This file is part of JTeroTeroStA.
 *
 * JTeroTeroStA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * JTeroTeroStA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with JTeroTeroStA. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/org/publishing_systems/_20140527t120137z/jteroterosta/JTeroThenCase.java
 * @author Stephan Kreutzer
 * @since 2021-10-15
 */

package org.publishing_systems._20140527t120137z.jteroterosta;



import java.util.List;



public class JTeroThenCase
{
    public JTeroThenCase(List<JTeroPattern> patterns,
                         String thenCaseCode,
                         String thenCaseFunction)
    {
        this.patterns = patterns;
        this.thenCaseCode = thenCaseCode;
        this.thenCaseFunction = thenCaseFunction;
    }

    public List<JTeroPattern> getPatterns()
    {
        return this.patterns;
    }

    public String getThenCaseCode()
    {
        return this.thenCaseCode;
    }

    public String getThenCaseFunction()
    {
        return this.thenCaseFunction;
    }

    protected List<JTeroPattern> patterns = null;
    protected String thenCaseCode = null;
    protected String thenCaseFunction = null;
}

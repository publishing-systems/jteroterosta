/* Copyright (C) 2021-2023 Stephan Kreutzer
 *
 * This file is part of JTeroTeroStA.
 *
 * JTeroTeroStA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * JTeroTeroStA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with JTeroTeroStA. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/org/publishing_systems/_20140527t120137z/jteroterosta/JTeroStAInterpreter.java
 * @author Stephan Kreutzer
 * @since 2021-10-26
 */

package org.publishing_systems._20140527t120137z.jteroterosta;



import java.io.InputStream;
import java.util.Map;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Iterator;



public class JTeroStAInterpreter
{
    public JTeroStAInterpreter(Map<String, JTeroFunction> functions,
                               Map<String, JTeroPattern> patterns,
                               String currentFunction,
                               JTeroInputStreamInterface stream,
                               boolean async,
                               String grammarName) throws JTeroException
    {
        if (functions == null)
        {
            throw new JTeroException(new IllegalArgumentException());
        }

        if (patterns == null)
        {
            throw new JTeroException(new IllegalArgumentException());
        }

        if (currentFunction == null)
        {
            throw new JTeroException(new IllegalArgumentException());
        }

        if (stream == null)
        {
            throw new JTeroException(new IllegalArgumentException());
        }

        this.functions = functions;
        this.patterns = patterns;
        this.functionCurrent = currentFunction;
        this.stream = stream;
        this.async = async;
        this.grammarName = grammarName;
    }

    public boolean hasNext() throws JTeroException
    {
        if (this.events.size() > 0)
        {
            return true;
        }

        if (this.hasNextCalled == true)
        {
            return false;
        }
        else
        {
            this.hasNextCalled = true;
        }

        if (this.stream.eof() == true)
        {
            return false;
        }

        int result = parse();

        do
        {
            if (result == 2)
            {
                result = parse();
                continue;
            }
            else if (result == 1)
            {
                break;
            }
            else if (result == 0)
            {
                break;
            }
            else if (result < 0)
            {
                return false;
            }
            else
            {
                throw new JTeroException("Unknown return code " + result + " not supported.");
            }

        } while (true);

        return this.events.size() > 0;
    }

    public JTeroEvent nextEvent() throws JTeroException
    {
        if (this.events.size() <= 0 &&
            this.hasNextCalled == false)
        {
            if (hasNext() != true)
            {
                throw new JTeroException("Attempted JTeroStAInterpreter.nextEvent() while there isn't one instead of checking JTeroStAInterpreter.hasNext() first.");
            }
        }

        this.hasNextCalled = false;

        if (this.events.size() <= 0)
        {
            throw new JTeroException("JTeroStAInterpreter.nextEvent() while there isn't one, ignoring JTeroStAInterpreter.hasNext() == false.");
        }

        return this.events.removeFirst();
    }

    protected int parse() throws JTeroException
    {
        JTeroFunction function = this.functions.get(this.functionCurrent);

        if (function == null)
        {
            throw new JTeroException("Call of unknown function \"" + this.functionCurrent + "\".");
        }

        /** @todo The matching of multiple then-cases, multiple patterns, multiple rules is implemented very
          * inefficiently (building/resetting thenCases and removing candidates every time), but initially
          * left this way for clarity of and illustrating the concept. State automaton could stay at the current
          * state/function if no match/transition is reached. */
        // <then-case-position, pattern-position, rule-position, rule>
        Map<Integer, Map<Integer, Map<Integer, JTeroRule>>> thenCases = new HashMap<Integer, Map<Integer, Map<Integer, JTeroRule>>>();

        {
            Integer i = 0;
            // NullPointerException here if function == null because into this.functions, no this.functionCurrent was loaded/found.
            Iterator<JTeroThenCase> iterThenCaseSource = function.getThenCases().iterator();

            while (iterThenCaseSource.hasNext() == true)
            {
                JTeroThenCase thenCaseSource = iterThenCaseSource.next();
                Map<Integer, Map<Integer, JTeroRule>> thenCaseTarget = new HashMap<Integer, Map<Integer, JTeroRule>>();

                thenCases.put(i, thenCaseTarget);

                Integer j = 0;
                Iterator<JTeroPattern> iterPatternSource = thenCaseSource.getPatterns().iterator();

                while (iterPatternSource.hasNext() == true)
                {
                    JTeroPattern patternSource = iterPatternSource.next();
                    Map<Integer, JTeroRule> patternTarget = new HashMap<Integer, JTeroRule>();

                    thenCaseTarget.put(j, patternTarget);

                    Integer k = 0;
                    Iterator<JTeroRule> iterRuleSource = patternSource.getRules().iterator();

                    while (iterRuleSource.hasNext() == true)
                    {
                        JTeroRule ruleSource = iterRuleSource.next();

                        ruleSource.reset();
                        patternTarget.put(k, ruleSource);

                        ++k;
                    }

                    ++j;
                }

                ++i;
            }
        }


        StringBuilder buffer = new StringBuilder();
        Map.Entry<Integer, Map<Integer, Map<Integer, JTeroRule>>> thenCaseMatch = null;
        Integer patternMatch = -1;

        String patternNameMatch = null;

        int inputCount = 0;

        do
        {
            InputInterface input = this.stream.get();

            if (this.stream.eof() == true)
            {
                if (this.async == false &&
                    this.functionPrevious.equals(this.functionCurrent) == true)
                {
                    /** @todo This might be incomplete/incorrect (not filling the actual data)? */
                    this.events.add(new JTeroEvent(this.functionCurrent,
                                                   null,
                                                   this.functionPreviousRepeatPerElseCase,
                                                   null,
                                                   null,
                                                   false,
                                                   this.matchCount,
                                                   inputCount,
                                                   this.collector.toString()));

                    this.collector = new StringBuilder();
                    this.patternPrevious = null;
                    this.functionPreviousRepeatPerElseCase = false;
                    this.matchCount = 0;
                }

                return 1;
            }

            String inputString = input.get();

            buffer.append(inputString);
            inputCount += inputString.length();


            for (Iterator<Map.Entry<Integer, Map<Integer, Map<Integer, JTeroRule>>>> iterThenCase = thenCases.entrySet().iterator(); iterThenCase.hasNext() == true; )
            {
                Map.Entry<Integer, Map<Integer, Map<Integer, JTeroRule>>> thenCase = iterThenCase.next();

                for (Iterator<Map.Entry<Integer, Map<Integer, JTeroRule>>> iterPattern = thenCase.getValue().entrySet().iterator(); iterPattern.hasNext() == true; )
                {
                    Map.Entry<Integer, Map<Integer, JTeroRule>> pattern = iterPattern.next();

                    for (Iterator<Map.Entry<Integer, JTeroRule>> iterRule = pattern.getValue().entrySet().iterator(); iterRule.hasNext() == true; )
                    {
                        Map.Entry<Integer, JTeroRule> rule = iterRule.next();

                        int comparisonResult = rule.getValue().compare(input);

                        if (comparisonResult == JTeroRule.RETURNVALUE_COMPARE_MATCH)
                        {
                            thenCaseMatch = thenCase;

                            patternMatch = pattern.getKey();
                            patternNameMatch = function.getThenCases().get(thenCase.getKey()).getPatterns().get(patternMatch).getName();

                            if (this.functionCurrent.equals(function.getThenCases().get(thenCase.getKey()).getThenCaseFunction()) == true)
                            {
                                this.matchCount += rule.getValue().getSequence().length();

                                this.patternPrevious = patternNameMatch;
                            }

                            break;
                        }
                        else if (comparisonResult == JTeroRule.RETURNVALUE_COMPARE_MATCHING)
                        {
                            continue;
                        }
                        else if (comparisonResult == JTeroRule.RETURNVALUE_COMPARE_MISMATCH)
                        {
                            iterRule.remove();
                            continue;
                        }
                        else
                        {
                            throw new JTeroException("Unknown return code " + comparisonResult + " not supported.");
                        }
                    }

                    if (thenCaseMatch != null)
                    {
                        break;
                    }

                    if (pattern.getValue().isEmpty() == true)
                    {
                        iterPattern.remove();
                        continue;
                    }
                }

                if (thenCaseMatch != null)
                {
                    break;
                }

                if (thenCase.getValue().isEmpty() == true)
                {
                    iterThenCase.remove();
                    continue;
                }
            }

            if (thenCaseMatch != null)
            {
                break;
            }

            if (thenCases.isEmpty() == true)
            {
                this.matchCount += inputCount;

                break;
            }

        } while (true);


        this.functionPrevious = this.functionCurrent;

        boolean nextFunctionPerElseCase = false;

        if (thenCaseMatch != null)
        {
            JTeroThenCase thenCase = null;

            {
                Integer i = 0;
                Iterator<JTeroThenCase> iterThenCaseSource = function.getThenCases().iterator();

                /** @todo Maybe this isn't necessary, because thenCase can already be
                  * obtained above in case JTeroRule.RETURNVALUE_COMPARE_MATCH? */
                while (iterThenCaseSource.hasNext() == true)
                {
                    JTeroThenCase thenCaseSource = iterThenCaseSource.next();

                    if (i == thenCaseMatch.getKey())
                    {
                        thenCase = thenCaseSource;
                        break;
                    }

                    ++i;
                }
            }

            String thenCaseCode = thenCase.getThenCaseCode();

            /** @todo This is just a temporary quick solution (as it prevents literal dot). */
            if (thenCaseCode.equals(".") == true)
            {
                this.collector.append(buffer);
            }
            else
            {

            }

            this.functionCurrent = thenCase.getThenCaseFunction();
        }
        else
        {
            String elseCaseCode = function.getElseCaseCode();

            /** @todo This is just a temporary quick solution (as it prevents literal dot). */
            if (elseCaseCode.equals(".") == true)
            {
                this.collector.append(buffer);
            }
            else
            {
                if (function.getHasRetain() == true)
                {
                    /** @todo buffer, this.collector and this.matchCount
                      * are still text-based and could/should be replaced
                      * with List<InputInterface>, so the retain mechanism
                      * can work purely symbolic and also retain/provide
                      * the underlying type of InputChar or InputTero,
                      * instead of resorting/downgrading to String (which
                      * is a remnant from prior pure text processing). */
                    for (int i = 0, max = buffer.length(); i < max; i++)
                    {
                        this.stream.push(new InputChar(buffer.charAt(i)));
                    }

                    this.matchCount -= buffer.length();
                    buffer = new StringBuilder();
                    inputCount = 0;
                }
            }

            this.functionCurrent = function.getElseCaseFunction();

            if (this.functionPrevious.equals(this.functionCurrent) == true)
            {
                this.functionPreviousRepeatPerElseCase = true;
            }
            else
            {
                nextFunctionPerElseCase = true;
            }
        }

        if (this.async == false)
        {
            if (this.functionPrevious.equals(this.functionCurrent) != true)
            {
                JTeroEvent event = new JTeroEvent(this.functionPrevious,
                                                  this.patternPrevious,
                                                  this.functionPreviousRepeatPerElseCase,
                                                  this.functionCurrent,
                                                  patternNameMatch,
                                                  nextFunctionPerElseCase,
                                                  this.matchCount,
                                                  inputCount,
                                                  this.collector.toString());
                this.events.add(event);

                this.collector = new StringBuilder();
                this.patternPrevious = null;
                this.functionPreviousRepeatPerElseCase = false;
                this.matchCount = 0;

                return 0;
            }
            else
            {
                return 2;
            }
        }
        else
        {
            JTeroEvent event = new JTeroEvent(this.functionPrevious,
                                              this.patternPrevious,
                                              this.functionPreviousRepeatPerElseCase,
                                              this.functionCurrent,
                                              patternNameMatch,
                                              nextFunctionPerElseCase,
                                              this.matchCount,
                                              inputCount,
                                              this.collector.toString());
            this.events.add(event);

            this.collector = new StringBuilder();
            this.patternPrevious = null;
            this.functionPreviousRepeatPerElseCase = false;
            this.matchCount = 0;

            return 0;
        }
    }

    protected boolean hasNextCalled = false;
    protected LinkedList<JTeroEvent> events = new LinkedList<JTeroEvent>();
    JTeroInputStreamInterface stream = null;

    protected Map<String, JTeroFunction> functions = null;
    protected Map<String, JTeroPattern> patterns = null;
    protected boolean async = false;
    protected String grammarName = null;

    protected String functionCurrent = null;
    protected String functionPrevious = null;
    protected String patternPrevious = null;
    protected boolean functionPreviousRepeatPerElseCase = false;
    StringBuilder collector = new StringBuilder();
    protected int matchCount = 0;
}

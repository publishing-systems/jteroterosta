/* Copyright (C) 2021-2022 Stephan Kreutzer
 *
 * This file is part of JTeroTeroStA.
 *
 * JTeroTeroStA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * JTeroTeroStA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with JTeroTeroStA. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/org/publishing_systems/_20140527t120137z/jteroterosta/JTeroException.java
 * @author Stephan Kreutzer
 * @since 2021-11-11
 */

package org.publishing_systems._20140527t120137z.jteroterosta;



public class JTeroException extends Exception
{
    public JTeroException(String message)
    {
        super(message);
    }

    public JTeroException(Throwable cause)
    {
        super(cause);
    }

    public JTeroException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public String getMessage()
    {
        return super.getMessage();
    }

    public Throwable getCause()
    {
        return super.getCause();
    }
}

/* Copyright (C) 2022 Stephan Kreutzer
 *
 * This file is part of JTeroTeroStA.
 *
 * JTeroTeroStA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * JTeroTeroStA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with JTeroTeroStA. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/org/publishing_systems/_20140527t120137z/jteroterosta/InputInterface.java
 * @author Stephan Kreutzer
 * @since 2022-02-20
 */

package org.publishing_systems._20140527t120137z.jteroterosta;



public interface InputInterface
{
    /**
     * @todo Could/should be much more intelligent (using injection
     *     or polymorphism, or instead of comparing the InputTero as
     *     a string sequence symbol, could compare for equality
     *     symbolically (unique numerical equivalent/representation).
     *     Also, String is just an initial lazy shortcut. Eventually,
     *     what's returned should retain the full type so it can be
     *     passed into type-aware/overloaded (symbolic) comparators.
     */
    public String get();
};

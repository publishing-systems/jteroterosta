/* Copyright (C) 2021-2022 Stephan Kreutzer
 *
 * This file is part of JTeroTeroStA.
 *
 * JTeroTeroStA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * JTeroTeroStA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with JTeroTeroStA. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/org/publishing_systems/_20140527t120137z/jteroterosta/JTeroSequence.java
 * @author Stephan Kreutzer
 * @since 2021-10-14
 */

package org.publishing_systems._20140527t120137z.jteroterosta;



public class JTeroSequence implements JTeroRule
{
    public JTeroSequence(String sequence) throws JTeroException
    {
        if (sequence == null)
        {
            throw new JTeroException(new IllegalArgumentException());
        }

        this.sequence = sequence;
    }

    public int compare(InputInterface input) throws JTeroException
    {
        String inputString = input.get();

        if (input instanceof InputChar)
        {
            if (inputString.length() != 1)
            {
                throw new JTeroException("Character represented by a String with a different length than 1.");
            }

            if (comparisonPosition == -1)
            {
                comparisonPosition = 0;
            }
            else if (comparisonPosition == this.sequence.length())
            {
                return JTeroRule.RETURNVALUE_COMPARE_MATCH;
            }

            if (inputString.charAt(0) != this.sequence.charAt(comparisonPosition))
            {
                comparisonPosition = -1;
                return JTeroRule.RETURNVALUE_COMPARE_MISMATCH;
            }
            else
            {
                comparisonPosition += 1;

                if (comparisonPosition == this.sequence.length())
                {
                    return JTeroRule.RETURNVALUE_COMPARE_MATCH;
                }
                else
                {
                    return JTeroRule.RETURNVALUE_COMPARE_MATCHING;
                }
            }
        }
        else if (input instanceof InputTero)
        {
            if (inputString.equals(this.sequence) == true)
            {
                return JTeroRule.RETURNVALUE_COMPARE_MATCH;
            }
            else
            {
                return JTeroRule.RETURNVALUE_COMPARE_MISMATCH;
            }
        }
        else
        {
            throw new JTeroException("Input type not supported.");
        }
    }

    public int reset()
    {
        comparisonPosition = -1;
        return 0;
    }

    public String getSequence()
    {
        return this.sequence;
    }

    protected String sequence = null;
    protected int comparisonPosition = -1;
}

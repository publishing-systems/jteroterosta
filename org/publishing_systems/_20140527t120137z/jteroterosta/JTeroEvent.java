/* Copyright (C) 2021-2022 Stephan Kreutzer
 *
 * This file is part of JTeroTeroStA.
 *
 * JTeroTeroStA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * JTeroTeroStA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with JTeroTeroStA. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/org/publishing_systems/_20140527t120137z/jteroterosta/JTeroEvent.java
 * @author Stephan Kreutzer
 * @since 2021-11-06
 */

package org.publishing_systems._20140527t120137z.jteroterosta;



public class JTeroEvent
{
    public JTeroEvent(String currentFunctionName,
                      String currentFunctionRepeatPatternName,
                      boolean currentFunctionRepeatElseCase,
                      String nextFunctionName,
                      String nextFunctionPatternName,
                      boolean nextFunctionElseCase,
                      int currentCount,
                      int nextCount,
                      String data)
    {
        this.currentFunctionName = currentFunctionName;
        this.currentFunctionRepeatPatternName = currentFunctionRepeatPatternName;
        this.currentFunctionRepeatElseCase = currentFunctionRepeatElseCase;

        this.nextFunctionName = nextFunctionName;
        this.nextFunctionPatternName = nextFunctionPatternName;
        this.nextFunctionElseCase = nextFunctionElseCase;

        this.currentCount = currentCount;
        this.nextCount = nextCount;

        this.data = data;
    }

    public String getCurrentFunctionName()
    {
        return this.currentFunctionName;
    }

    public String getCurrentFunctionRepeatPatternName()
    {
        return this.currentFunctionRepeatPatternName;
    }

    public boolean getCurrentFunctionRepeatElseCase()
    {
        return this.currentFunctionRepeatElseCase;
    }

    public String getNextFunctionName()
    {
        return this.nextFunctionName;
    }

    public String getNextFunctionPatternName()
    {
        return this.nextFunctionPatternName;
    }

    public boolean getNextFunctionElseCase()
    {
        return this.nextFunctionElseCase;
    }

    public int getCurrentCount()
    {
        return this.currentCount;
    }

    public int getNextCount()
    {
        return this.nextCount;
    }

    public String getData()
    {
        return this.data;
    }

    protected String currentFunctionName = null;
    protected String currentFunctionRepeatPatternName = null;
    protected boolean currentFunctionRepeatElseCase = false;

    protected String nextFunctionName = null;
    protected String nextFunctionPatternName = null;
    protected boolean nextFunctionElseCase = false;

    protected int currentCount = -1;
    protected int nextCount = -1;

    protected String data = null;
}

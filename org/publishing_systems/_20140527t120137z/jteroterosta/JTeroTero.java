/* Copyright (C) 2021-2022 Stephan Kreutzer
 *
 * This file is part of JTeroTeroStA.
 *
 * JTeroTeroStA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * JTeroTeroStA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with JTeroTeroStA. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/org/publishing_systems/_20140527t120137z/jteroterosta/JTeroTero.java
 * @author Stephan Kreutzer
 * @since 2022-02-05
 */

package org.publishing_systems._20140527t120137z.jteroterosta;



import java.util.List;
import java.util.Map;



public class JTeroTero
{
    public JTeroTero(List<JTeroLoader> loaders, JTeroInputStreamInterface inputStream) throws JTeroException
    {
        if (loaders == null)
        {
            throw new JTeroException(new IllegalArgumentException());
        }

        if (loaders.size() <= 0)
        {
            throw new JTeroException(new IllegalArgumentException());
        }

        this.loaders = loaders;
        this.inputStream = inputStream;
    }

    public boolean hasNext() throws JTeroException
    {
        if (this.internalInterpreter == null)
        {
            if (this.inputStream == null)
            {
                throw new JTeroException("hasNext() called without passing an input stream to the constructor.");
            }

            this.internalInterpreter = setup(this.loaders.size() - 1);
            this.inputStream = null;
        }

        return this.internalInterpreter.hasNext();
    }

    public JTeroEvent nextEvent() throws JTeroException
    {
        if (this.internalInterpreter == null)
        {
            throw new JTeroException("nextEvent() called without prior check for hasNext().");
        }

        return this.internalInterpreter.nextEvent();
    }

    public int parse(JTeroInputStreamInterface inputStream) throws JTeroException
    {
        if (inputStream == null)
        {
            throw new JTeroException(new IllegalArgumentException());
        }

        if (this.internalInterpreter != null)
        {
            throw new JTeroException("parse() called while already in hasNext()/nextEvent() mode.");
        }

        this.inputStream = inputStream;
        JTeroStAInterpreter interpreter = setup(this.loaders.size() - 1);
        this.inputStream = null;

        while (interpreter.hasNext() == true)
        {
            JTeroEvent teroEvent = interpreter.nextEvent();

            System.out.print(teroEvent.getData());
        }

        return 0;
    }

    protected JTeroStAInterpreter setup(int index) throws JTeroException
    {
        JTeroLoader loader = this.loaders.get(index);
        loader.load(true);

        Map<String, JTeroPattern> patterns = loader.getPatterns();
        Map<String, JTeroFunction> functions = loader.getFlow();

        if (index > 0)
        {
            JTeroStAInterpreter interpreter = setup(index - 1);
            // Wrap obtained lower-level interpreter behind stream interface.
            JTeroInputStreamInterface stream = new JTeroInputStreamTero(interpreter);

            // Return this setup's own interpreter with the stream wrapper passed into it as it's source.
            return new JTeroStAInterpreter(functions, patterns, "InMain", stream, false, loader.getCodeDirectoryPath());
        }
        else if (index == 0)
        {
            JTeroStAInterpreter interpreter = new JTeroStAInterpreter(functions, patterns, "InMain", this.inputStream, false, loader.getCodeDirectoryPath());
            this.inputStream = null;

            return interpreter;
        }
        else
        {
            throw new JTeroException(new IllegalArgumentException());
        }
    }

    protected List<JTeroLoader> loaders = null;
    protected JTeroInputStreamInterface inputStream = null;
    protected JTeroStAInterpreter internalInterpreter = null;
}

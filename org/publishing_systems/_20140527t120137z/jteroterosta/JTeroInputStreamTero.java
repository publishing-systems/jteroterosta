/* Copyright (C) 2021-2022 Stephan Kreutzer
 *
 * This file is part of JTeroTeroStA.
 *
 * JTeroTeroStA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * JTeroTeroStA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with JTeroTeroStA. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/org/publishing_systems/_20140527t120137z/jteroterosta/JTeroInputStreamTero.java
 * @author Stephan Kreutzer
 * @since 2022-02-05
 */

package org.publishing_systems._20140527t120137z.jteroterosta;



import java.util.List;
import java.util.ArrayList;



public class JTeroInputStreamTero implements JTeroInputStreamInterface
{
    public JTeroInputStreamTero(JTeroStAInterpreter interpreter) throws JTeroException
    {
        if (interpreter == null)
        {
            throw new JTeroException(new IllegalArgumentException());
        }

        this.interpreter = interpreter;
    }

    public InputInterface get() throws JTeroException
    {
        if (this.eof == true)
        {
            return null;
        }

        if (this.buffer.isEmpty() == true)
        {
            if (this.interpreter.hasNext() == true)
            {
                JTeroEvent teroEvent = this.interpreter.nextEvent();

                return new InputTero(teroEvent);
            }
            else
            {
                this.eof = true;
                return null;
            }
        }
        else
        {
            InputInterface input = this.buffer.get(this.buffer.size() - 1);
            this.buffer.remove(this.buffer.size() - 1);

            return input;
        }
    }

    public boolean eof()
    {
        return this.eof;
    }

    public int push(InputInterface input)
    {
        if (this.eof == true)
        {
            return -1;
        }

        this.buffer.add(input);

        return 0;
    }

    protected JTeroStAInterpreter interpreter = null;

    protected List<InputInterface> buffer = new ArrayList<InputInterface>();

    protected boolean eof = false;
}

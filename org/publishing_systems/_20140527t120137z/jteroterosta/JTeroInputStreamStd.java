/* Copyright (C) 2021-2022 Stephan Kreutzer
 *
 * This file is part of JTeroTeroStA.
 *
 * JTeroTeroStA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * JTeroTeroStA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with JTeroTeroStA. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/org/publishing_systems/_20140527t120137z/jteroterosta/JTeroInputStreamStd.java
 * @author Stephan Kreutzer
 * @since 2021-11-06
 */

package org.publishing_systems._20140527t120137z.jteroterosta;



import java.io.InputStream;
import java.io.BufferedReader;
import java.util.List;
import java.util.ArrayList;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.io.IOException;



public class JTeroInputStreamStd implements JTeroInputStreamInterface
{
    public JTeroInputStreamStd(InputStream stream) throws JTeroException
    {
        if (stream == null)
        {
            throw new JTeroException(new IllegalArgumentException());
        }

        try
        {
            this.stream = new BufferedReader(new InputStreamReader(stream, "UTF-8"));
        }
        catch (UnsupportedEncodingException ex)
        {
            this.stream = null;
            throw new JTeroException(ex);
        }
    }

    public InputInterface get() throws JTeroException
    {
        if (this.eof == true)
        {
            return null;
        }

        if (this.buffer.isEmpty() == true)
        {
            int character = -1;

            try
            {
                character = this.stream.read();
            }
            catch (IOException ex)
            {
                throw new JTeroException(ex);
            }

            if (character < 0)
            {
                this.eof = true;
                return null;
            }
            else
            {
                return new InputChar((char)character);
            }
        }
        else
        {
            InputInterface input = this.buffer.get(this.buffer.size() - 1);
            this.buffer.remove(this.buffer.size() - 1);

            return input;
        }
    }

    public boolean eof()
    {
        return this.eof;
    }

    public int push(InputInterface input)
    {
        if (this.eof == true)
        {
            return -1;
        }

        this.buffer.add(input);

        return 0;
    }

    protected BufferedReader stream = null;

    List<InputInterface> buffer = new ArrayList<InputInterface>();

    protected boolean eof = false;
}

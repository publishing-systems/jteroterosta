# Copyright (C) 2021-2022  Stephan Kreutzer
#
# This file is part of JTeroTeroStA.
#
# JTeroTeroStA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License version 3 or any later version,
# as published by the Free Software Foundation.
#
# JTeroTeroStA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License 3 for more details.
#
# You should have received a copy of the GNU Affero General Public License 3
# along with JTeroTeroStA. If not, see <http://www.gnu.org/licenses/>.



.PHONY: all JTeroTeroStA clean



all: JTeroTeroStA
JTeroTeroStA: JTeroTeroStA.class



org/publishing_systems/_20140527t120137z/jteroterosta/JTeroException.class: org/publishing_systems/_20140527t120137z/jteroterosta/JTeroException.java
	javac -encoding UTF-8 org/publishing_systems/_20140527t120137z/jteroterosta/JTeroException.java

org/publishing_systems/_20140527t120137z/jteroterosta/InputInterface.class: org/publishing_systems/_20140527t120137z/jteroterosta/InputInterface.java
	javac -encoding UTF-8 org/publishing_systems/_20140527t120137z/jteroterosta/InputInterface.java

org/publishing_systems/_20140527t120137z/jteroterosta/InputChar.class: org/publishing_systems/_20140527t120137z/jteroterosta/InputChar.java
	javac -encoding UTF-8 org/publishing_systems/_20140527t120137z/jteroterosta/InputChar.java

org/publishing_systems/_20140527t120137z/jteroterosta/InputTero.class: org/publishing_systems/_20140527t120137z/jteroterosta/InputTero.java
	javac -encoding UTF-8 org/publishing_systems/_20140527t120137z/jteroterosta/InputTero.java

org/publishing_systems/_20140527t120137z/jteroterosta/JTeroInputStreamInterface.class: org/publishing_systems/_20140527t120137z/jteroterosta/JTeroInputStreamInterface.java
	javac -encoding UTF-8 org/publishing_systems/_20140527t120137z/jteroterosta/JTeroInputStreamInterface.java

org/publishing_systems/_20140527t120137z/jteroterosta/JTeroInputStreamStd.class: org/publishing_systems/_20140527t120137z/jteroterosta/JTeroInputStreamStd.java
	javac -encoding UTF-8 org/publishing_systems/_20140527t120137z/jteroterosta/JTeroInputStreamStd.java

org/publishing_systems/_20140527t120137z/jteroterosta/JTeroInputStreamTero.class: org/publishing_systems/_20140527t120137z/jteroterosta/JTeroInputStreamTero.java
	javac -encoding UTF-8 org/publishing_systems/_20140527t120137z/jteroterosta/JTeroInputStreamTero.java

org/publishing_systems/_20140527t120137z/jteroterosta/JTeroRule.class: org/publishing_systems/_20140527t120137z/jteroterosta/JTeroRule.java
	javac -encoding UTF-8 org/publishing_systems/_20140527t120137z/jteroterosta/JTeroRule.java

org/publishing_systems/_20140527t120137z/jteroterosta/JTeroSequence.class: org/publishing_systems/_20140527t120137z/jteroterosta/JTeroSequence.java
	javac -encoding UTF-8 org/publishing_systems/_20140527t120137z/jteroterosta/JTeroSequence.java

org/publishing_systems/_20140527t120137z/jteroterosta/JTeroPattern.class: org/publishing_systems/_20140527t120137z/jteroterosta/JTeroPattern.java
	javac -encoding UTF-8 org/publishing_systems/_20140527t120137z/jteroterosta/JTeroPattern.java

org/publishing_systems/_20140527t120137z/jteroterosta/JTeroThenCase.class: org/publishing_systems/_20140527t120137z/jteroterosta/JTeroThenCase.java org/publishing_systems/_20140527t120137z/jteroterosta/JTeroPattern.class
	javac -encoding UTF-8 org/publishing_systems/_20140527t120137z/jteroterosta/JTeroThenCase.java

org/publishing_systems/_20140527t120137z/jteroterosta/JTeroFunction.class: org/publishing_systems/_20140527t120137z/jteroterosta/JTeroFunction.java org/publishing_systems/_20140527t120137z/jteroterosta/JTeroThenCase.class
	javac -encoding UTF-8 org/publishing_systems/_20140527t120137z/jteroterosta/JTeroFunction.java

org/publishing_systems/_20140527t120137z/jteroterosta/JTeroLoader.class: org/publishing_systems/_20140527t120137z/jteroterosta/JTeroLoader.java
	javac -encoding UTF-8 org/publishing_systems/_20140527t120137z/jteroterosta/JTeroLoader.java

org/publishing_systems/_20140527t120137z/jteroterosta/JTeroEvent.class: org/publishing_systems/_20140527t120137z/jteroterosta/JTeroEvent.java
	javac -encoding UTF-8 org/publishing_systems/_20140527t120137z/jteroterosta/JTeroEvent.java

org/publishing_systems/_20140527t120137z/jteroterosta/JTeroStAInterpreter.class: org/publishing_systems/_20140527t120137z/jteroterosta/JTeroStAInterpreter.java
	javac -encoding UTF-8 org/publishing_systems/_20140527t120137z/jteroterosta/JTeroStAInterpreter.java

org/publishing_systems/_20140527t120137z/jteroterosta/JTeroTero.class: org/publishing_systems/_20140527t120137z/jteroterosta/JTeroTero.java
	javac -encoding UTF-8 org/publishing_systems/_20140527t120137z/jteroterosta/JTeroTero.java

JTeroTeroStA.class: JTeroTeroStA.java org/publishing_systems/_20140527t120137z/jteroterosta/JTeroException.class org/publishing_systems/_20140527t120137z/jteroterosta/InputInterface.class org/publishing_systems/_20140527t120137z/jteroterosta/InputChar.class org/publishing_systems/_20140527t120137z/jteroterosta/InputTero.class org/publishing_systems/_20140527t120137z/jteroterosta/JTeroInputStreamInterface.class org/publishing_systems/_20140527t120137z/jteroterosta/JTeroInputStreamStd.class org/publishing_systems/_20140527t120137z/jteroterosta/JTeroInputStreamTero.class org/publishing_systems/_20140527t120137z/jteroterosta/JTeroRule.class org/publishing_systems/_20140527t120137z/jteroterosta/JTeroSequence.class org/publishing_systems/_20140527t120137z/jteroterosta/JTeroPattern.class org/publishing_systems/_20140527t120137z/jteroterosta/JTeroThenCase.class org/publishing_systems/_20140527t120137z/jteroterosta/JTeroFunction.class org/publishing_systems/_20140527t120137z/jteroterosta/JTeroLoader.class org/publishing_systems/_20140527t120137z/jteroterosta/JTeroEvent.class org/publishing_systems/_20140527t120137z/jteroterosta/JTeroStAInterpreter.class org/publishing_systems/_20140527t120137z/jteroterosta/JTeroTero.class
	javac -encoding UTF-8 JTeroTeroStA.java

clean:
	rm -f JTeroTeroStA.class
	rm -f org/publishing_systems/_20140527t120137z/jteroterosta/JTeroTero.class
	rm -f org/publishing_systems/_20140527t120137z/jteroterosta/JTeroStAInterpreter.class
	rm -f org/publishing_systems/_20140527t120137z/jteroterosta/JTeroEvent.class
	rm -f org/publishing_systems/_20140527t120137z/jteroterosta/JTeroLoader.class
	rm -f org/publishing_systems/_20140527t120137z/jteroterosta/JTeroFunction.class
	rm -f org/publishing_systems/_20140527t120137z/jteroterosta/JTeroThenCase.class
	rm -f org/publishing_systems/_20140527t120137z/jteroterosta/JTeroPattern.class
	rm -f org/publishing_systems/_20140527t120137z/jteroterosta/JTeroSequence.class
	rm -f org/publishing_systems/_20140527t120137z/jteroterosta/JTeroRule.class
	rm -f org/publishing_systems/_20140527t120137z/jteroterosta/JTeroInputStreamTero.class
	rm -f org/publishing_systems/_20140527t120137z/jteroterosta/JTeroInputStreamStd.class
	rm -f org/publishing_systems/_20140527t120137z/jteroterosta/JTeroInputStreamInterface.class
	rm -f org/publishing_systems/_20140527t120137z/jteroterosta/InputTero.class
	rm -f org/publishing_systems/_20140527t120137z/jteroterosta/InputChar.class
	rm -f org/publishing_systems/_20140527t120137z/jteroterosta/InputInterface.class
	rm -f org/publishing_systems/_20140527t120137z/jteroterosta/JTeroException.class
